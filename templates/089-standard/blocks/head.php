<?php
/**
 * @author   	089webdesgin.de
 * @copyright   Copyright (C) 2015 089webdesgin.de. All rights reserved.
 * @URL 		https://089webdesgin.de/
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */


defined('_JEXEC') or die;

// Add JavaScript Frameworks
JHtml::_('bootstrap.framework');
$doc->addScript('templates/' . $this->template . '/js/template.js');
$doc->addScript('templates/' . $this->template . '/js/modernizr.custom.25376.js');
//$doc->addScript('templates/' . $this->template . '/js/menu.js');

// Add Stylesheets
$doc->addStyleSheet('templates/' . $this->template . '/css/normalize.css');
$doc->addStyleSheet('templates/' . $this->template . '/css/template.css');
$doc->addStyleSheet('templates/' . $this->template . '/css/responsive.css');


// Load optional RTL Bootstrap CSS
JHtml::_('bootstrap.loadCss', true, $this->direction);

// Add Stylesheets - CG overrides als letztes!!
$doc->addStyleSheet('templates/' . $this->template . '/css/overrides.css');

?>

	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<jdoc:include type="head" />
	<?php // Use of Google Font ?>
	<?php if ($this->params->get('googleFont')) : ?>
		<link href='//fonts.googleapis.com/css?family=<?php echo $this->params->get('googleFontName'); ?>' rel='stylesheet' type='text/css' />
		<style type="text/css">
			h1,h2,h3,h4,h5,h6,.site-title{
				font-family: '<?php echo str_replace('+', ' ', $this->params->get('googleFontName')); ?>', sans-serif;
			}
		</style>
	<?php endif; ?>
	
	<!--[if lt IE 9]>
		<script src="<?php echo $this->baseurl; ?>/media/jui/js/html5.js"></script>
	<![endif]-->

	<?php /* Bugehrdscript entfernen nach Go-Live
		<script type='text/javascript'>
			(function (d, t) {
			  var bh = d.createElement(t), s = d.getElementsByTagName(t)[0];
			  bh.type = 'text/javascript';
			  bh.src = '//www.bugherd.com/sidebarv2.js?apikey=lj88weoki87jlvua4uhsrw';
			  s.parentNode.insertBefore(bh, s);
			  })(document, 'script');
		</script>

		*/ ?>