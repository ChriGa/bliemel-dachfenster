<?php
/**
 * @author   	089webdesgin.de
 * @copyright   Copyright (C) 2015 089webdesgin.de. All rights reserved.
 * @URL 		https://089webdesgin.de/
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<footer class="clear-footer" role="footer">
	<div class="container clear-footer-wrap">		
		
		<div class="row-fluid">
			
					
			<?php if ($this->countModules('footnav')) : ?>
				<div class="footnav pull-right">
					<div class="module_footer position_footnav">
						<jdoc:include type="modules" name="footnav" style="none" />
					</div>			
				</div>
			<?php endif; ?>
			
			
			<div class="footer">
				<div class="f-slogan">
					<h4 class="pull-left"><span>Roto</span>Profipartner</h4>
				</div>
			</div>
			
			
		
		</div>
		
	</div>
</footer>
<div class="copyright">
	<p class="text-center">© 2015 Bliemel Dachfenster und Fassadenfenster M&uuml;nchen Gr&auml;felfing</p>
</div>
	
		