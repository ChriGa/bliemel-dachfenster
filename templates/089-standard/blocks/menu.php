<?php
/**
 * @author   	089webdesgin.de
 * @copyright   Copyright (C) 2015 089webdesgin.de. All rights reserved.
 * @URL 		https://089webdesgin.de/
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
 
defined('_JEXEC') or die;
?>


	<nav class="navbar-wrapper">
      <!-- Wrap the .navbar in .container to center it within the absolutely positioned parent. -->
      <div class="container menu-bkgr">
        <div class="navbar">
          <div class="navbar-inner">

			<div class="column hidden1024">
				<p>
					<button id="showMenu">
						Men&uuml; anzeigen
                	</button>
                </p>							
			</div>
                         
			  <?php if ($this->countModules('menu')) : ?>
				<div class="nav-collapse collapse visible1025"  role="navigation">
					<jdoc:include type="modules" name="menu" style="custom" />
				</div>
				<?php endif; ?>
			  
            <!--/.nav-collapse -->
          </div><!-- /.navbar-inner -->
        </div><!-- /.navbar -->

      </div> 
	  
	  
	  
	  <!-- /.container -->
    </nav>
