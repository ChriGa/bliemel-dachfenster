<?php
/**
 * @author   	089webdesgin.de
 * @copyright   Copyright (C) 2015 089webdesgin.de. All rights reserved.
 * @URL 		https://089webdesgin.de/
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
 
defined('_JEXEC') or die;
?>
<?php if ($this->countModules('slider')) : ?>
<div class="clear-slider">
	<div class="clear-slider-wrap">		
		<jdoc:include type="modules" name="slider" style="none" />		
	</div>
</div>
<?php endif; ?>