<?php
/**
 * @author   	089webdesgin.de
 * @copyright   Copyright (C) 2015 089webdesgin.de. All rights reserved.
 * @URL 		https://089webdesgin.de/
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
 
defined('_JEXEC') or die;
?>

<header class="header">
	<div class="header-wrapper">
      <!-- Wrap the .navbar in .container to center it within the absolutely positioned parent. -->
      <div class="container ">
		<div class="row-fluid">

           
			<div class="header-inner clearfix">
				<div class="span4 logo pull-left">
					<a class="brand" href="<?php echo $this->baseurl; ?>">
						<?php echo $logo; ?>
						<?php if ($this->params->get('sitedescription')) : ?>
							<?php echo '<div class="site-description">' . htmlspecialchars($this->params->get('sitedescription')) . '</div>'; ?>
						<?php endif; ?>
					</a>
				</div>
					
				<div class="header-right pull-right">
					<jdoc:include type="modules" name="head-right" style="custom" />
				</div>
			</div>   
            
			
			
			
            
        
		</div>
      </div> <!-- /.container -->
    </div>
</header>