<?php
/**
 * @author   	089webdesgin.de
 * @copyright   Copyright (C) 2015 089webdesgin.de. All rights reserved.
 * @URL 		https://089webdesgin.de/
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;


?>
<div class="clr"></div>
	<div class="clear-top">
		<div class="container container-top">

					<?php if ($this->countModules('top1')) : ?>
					<div class="module_top position_top"> 
						<jdoc:include type="modules" name="top1" style="xhtml" />
					</div>
					<?php endif; ?>
		</div> 
	</div>	
<div class="clr"></div>