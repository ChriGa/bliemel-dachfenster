<?php
/**
 * @author   	cg@089webdesign.de
 */
 

defined('_JEXEC') or die;
//include system
include_once(JPATH_ROOT . "/templates/" . $this->template . '/lib/system.php');
//include template Functions CG
include_once(JPATH_ROOT . "/templates/" . $this->template . '/template_functions.php');
//
?>
<!DOCTYPE html>
<html lang="de-de">
<head>
	<?php 
	// including head
	include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/head.php');
	?>

</head>

<body id="body" class="site <?php echo $option
	. ' view-' . $view
	. ($layout ? ' layout-' . $layout : ' no-layout')
	. ($task ? ' task-' . $task : ' no-task')
	. ($itemid ? ' itemid-' . $itemid : '')
	. $body_class . $detectAgent; print ($clientMobile) ? "mobile " : " ";
?>">

	<!-- Body -->
		<div id="perspective" class="perspective effect-moveleft">
			<div class="container-fx site_wrapper">
				<div class="wrapper">
					<?php			
					
					// including header
					include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/header.php');			

					// including header
					include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/menu.php');			
					
					// including slider
					include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/slider.php');

					// // including top
					include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/top.php');	

					// including top
					include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/breadcrumbs.php');						
												
					// // including top2
					// include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/top2.php');	
											
					// including content
					include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/content.php');	
					
					// including bottom
					include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/bottom.php');	
					
					// including bottom2
					//include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/bottom2.php');
					
					// including content
					include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/footer.php');				
					
					?>					
				</div><?//!!!?>
			</div>
			<div class="hidden1024 resp-menu">
				<?php if ($this->countModules('resp-menu')) : ?>
						<jdoc:include type="modules" name="resp-menu" style="custom" />
						<script>
							jQuery(document).ready(function(){
							    jQuery(".navOpen1").click(function(e){
							    	e.preventDefault();
							      if( !jQuery('.resp-nav-child1').hasClass("open") ) {
							        jQuery('.resp-nav-child1').addClass("open");
							        jQuery('.toggleChar1').addClass("btnMinus");						        
							    	return false;
							      } else if (jQuery('body').hasClass('openNav')) {
							      	jQuery('body').removeClass("openNav");
							      	jQuery('.resp-nav-child2, .resp-nav-child3').removeClass("open");
							      	jQuery('.toggleChar2, .toggleChar3').removeClass("btnMinus");						        
							    	return false;
							      }
							      else {
							        jQuery('.resp-nav-child1').removeClass("open");
							        jQuery('.toggleChar1').toggleClass("btnMinus");						        
							        return false;
							      } <?php // mit wenn hasClass 'open' append html (?)->glyphicons? ?>
							    });
							});
							jQuery(document).ready(function(){
							    jQuery(".navOpen2").click(function(e){
							    	e.preventDefault();
							      if( !jQuery('.resp-nav-child2').hasClass("open") ) {
							        jQuery('.resp-nav-child2').addClass("open");
							       	jQuery('body').addClass("openNav");
							       	jQuery('.toggleChar2').addClass("btnMinus");
							    	return false;
							      }
							      else {
							        jQuery('.resp-nav-child2').removeClass("open");
							        jQuery('body').removeClass("openNav");
							        jQuery('.toggleChar2').removeClass("btnMinus");							        
							        return false;
							      }
							    });
							});
							jQuery(document).ready(function(){
							    jQuery(".navOpen3").click(function(e){
							    	e.preventDefault();
							      if( !jQuery('.resp-nav-child3').hasClass("open") ) {
							        jQuery('.resp-nav-child3').addClass("open");
							       	jQuery('body').addClass("openNav2nd");
							       	jQuery('.toggleChar3').addClass("btnMinus");					        
							    	return false;
							      } else {
							        jQuery('.resp-nav-child3').removeClass("open");
							        jQuery('body').removeClass("openNav2nd");
							        jQuery('.toggleChar3').removeClass("btnMinus");								        
							        return false;
							      }
							    });
							});
							jQuery(document).ready(function(){
							    jQuery(".navOpen4").click(function(e){
							    	e.preventDefault();
							      if( !jQuery('.resp-nav-child4').hasClass("open") ) {
							        jQuery('.resp-nav-child4').addClass("open");
							        jQuery('.toggleChar4').addClass("btnMinus");
							    	return false;
							      }
							      else {
							        jQuery('.resp-nav-child4').removeClass("open");
							        jQuery('.toggleChar4').removeClass("btnMinus");
							        return false;
							      }
							    });
							});							
						</script>			
				<?php endif; ?>
			</div>
		</div>

<?php // scrollToTop ?>
<div class="scroll-top-wrapper ">
	<span class="scroll-top-inner">
		<span class="glyphicon glyphicon-chevron-up" aria-hidden="true"></span>
	</span>
</div>
<script>	 
 	jQuery(function(){	 
		jQuery(document).on( 'scroll', function(){
	 
			if (jQuery(window).scrollTop() > 100) {
				jQuery('.scroll-top-wrapper').addClass('show');
			} else {
				jQuery('.scroll-top-wrapper').removeClass('show');
			}
		});	 
		jQuery('.scroll-top-wrapper').on('click', scrollToTop);
	});	 
	function scrollToTop() {
		verticalOffset = typeof(verticalOffset) != 'undefined' ? verticalOffset : 0;
		element = jQuery('body');
		offset = element.offset();
		offsetTop = offset.top;
		jQuery('html, body').animate({scrollTop: offsetTop}, 500, 'linear');
	}
</script>

		<jdoc:include type="modules" name="debug" style="none" />
	<script src="<?php print "templates/". $this->template ?>/js/classie.js"></script>
	<script src="<?php print "templates/". $this->template ?>/js/menu_cd.js"></script>
<?php /*  CG NIVO Slider und Bootstrap conflict!: wird vorerst nich benötigt wenn Parameter im Modul jquery 1.7 laden-> nein, js in body, und conflict mode->no! gesetzt sind!
	
	<script type="text/javascript">
		 var j = jQuery.noConflict();
		 	j(window).load(function() {
		 	    j('#nivo_slider_91').nivoSlider({
		 	        slideshowEnd: function(){$('#nivo_slider_91').data('nivo:vars').stop = true;} 
		 	    });
		 	});
		 var i = jQuery.noConflict();
		 	i(window).load(function() {
		 	    i('#nivo_slider_93').nivoSlider({
			        slideshowEnd: function(){$('#nivo_slider_93').data('nivo:vars').stop = true;} 
			    });			    
			});
	</script>
 */ ?>
</body>
</html>
