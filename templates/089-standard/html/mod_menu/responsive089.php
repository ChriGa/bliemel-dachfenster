<?php
/**
 * @package     	Joomla.Site
 * @subpackage  	mod_menu override
 * @copyright   	Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     	GNU General Public License version 2 or later; see LICENSE.txt
 * Modifications	Joomla CSS
 */

defined('_JEXEC') or die;
// Note. It is important to remove spaces between elements.
?>
<?php // The menu class is deprecated. Use nav instead.

$navID = 1; ?>

	<nav class="outer-nav right vertical"<?php
		$tag = '';
		if ($params->get('tag_id') != null)
		{
			$tag = $params->get('tag_id').'';
			echo ' id="'.$tag.'"';
		}
	?>>
	<?php
	foreach ($list as $i => &$item) :
		$class = 'item-'.$item->id;
		if ($item->id == $active_id)
		{
			$class .= ' current';
		}

		if (in_array($item->id, $path))
		{
			$class .= ' active';
		}
		elseif ($item->type == 'alias')
		{
			$aliasToId = $item->params->get('aliasoptions');
			if (count($path) > 0 && $aliasToId == $path[count($path) - 1])
			{
				$class .= ' active';
			}
			elseif (in_array($aliasToId, $path))
			{
				$class .= ' alias-parent-active';
			}
		}

		if ($item->type == 'separator')
		{
			$class .= ' divider';
		}

		if ($item->deeper) {
			if ($item->level < 2) {
				$class .= ' dropdown deeper  mootools-noconflict';
				$first_start = true;
			}
			else {
				$class .= ' dropdown-submenu deeper';
			}
		}

		if ($item->parent)
		{
			$class .= ' parent';
		}

		if (!empty($class))
		{
			$class = ' class="'.trim($class) .'"';
		}

		echo '<a'.$class;		
		// Render the menu item.
		switch ($item->type) :
			case 'separator':
			case 'url':
			case 'component':
			case 'heading':
				require JModuleHelper::getLayoutPath('mod_menu', 'default_mobile089');
				break;

			default:
				require JModuleHelper::getLayoutPath('mod_menu', 'default_url');
				break;
		endswitch;

		// The next item is deeper.
		
		if ($item->deeper){
			echo '<button class="btn navOpen'.$navID.'" type="button" id="navID-'.$navID.'"><span class="toggleChar'.$navID.' btnPlus"></span></button>
    				<nav class="resp-nav-child'.$navID.' unstyled small" id="navID-'.$navID.'">';
    				$navID ++;
		}
		elseif ($item->deeper) {
			echo '<nav>';
		}
		// The next item is shallower.
		elseif ($item->shallower) {
			echo '</a>';
			echo str_repeat('</nav></a>', $item->level_diff);
		}
		// The next item is on the same level.
		else {
			echo '</a>';
		}
	endforeach;
	?>
	</nav>