<?php

/**
 * Liefert den benutzten Browser zurück: IE, IE7, Firefox, Safari, usw
 * von http://www.php.net/manual/de/function.get-browser.php#101125
 *
 * @todo: Später evtl das hier verwenden? http://chrisschuld.com/projects/browser-php-detecting-a-users-browser-from-php.html
 *
 */
	function getBrowser() {
		$u_agent = $_SERVER['HTTP_USER_AGENT'];
		$ub = 'Unknown';
		$bname = 'Unknown';
		$platform = 'Unknown';
		$version= "";
		//preprint($u_agent); // IE11 = "Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; rv:11.0) like Gecko"
		
		 
		//First get the platform?
		if (preg_match('/linux/i', $u_agent)) {
			$platform = 'linux';
		}
		elseif (preg_match('/iPad/i', $u_agent)) {
			$platform = 'iPad';
		}
		elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
			$platform = 'mac';
		}
		elseif (preg_match('/windows|win32/i', $u_agent)) {
			$platform = 'windows';
		}
		
	   
		// Next get the name of the useragent yes seperately and for good reason
		if(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent))
		{
			$bname = 'IE';
			$ub = "MSIE";
		}
		elseif(preg_match('/Trident/i',$u_agent))
		{
			$bname = 'IE';
			$ub = "MSIE";
			
			$version = explode("rv:", $u_agent);
			$version = (float) $version[1];
		}
		elseif(preg_match('/Firefox/i',$u_agent))
		{
			$bname = 'Firefox';
			$ub = "Firefox";
		}
		elseif(preg_match('/Chrome/i',$u_agent))
		{
			$bname = 'Chrome';
			$ub = "Chrome";
		}
		elseif(preg_match('/Safari/i',$u_agent))
		{
			$bname = 'Safari';
			$ub = "Safari";
		}
		elseif(preg_match('/Opera/i',$u_agent))
		{
			$bname = 'Opera';
			$ub = "Opera";
		}
		elseif(preg_match('/Netscape/i',$u_agent))
		{
			$bname = 'Netscape';
			$ub = "Netscape";
		}

		$return = array(
			'userAgent' => $u_agent,
			'name'      => $bname,
			'platform'  => $platform
		);

		//preprint($return);
		return $return;
	}
?>